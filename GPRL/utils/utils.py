# Copyright (c) Mathurin Videau. All Rights Reserved.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

from functools import reduce
from operator import add, itemgetter
import pickle
import numpy as np
import pandas as pd
import os

def convert_logbook_to_dataframe(logbook):
    chapter_keys = logbook.chapters.keys()
    sub_chaper_keys = [c[0].keys() for c in logbook.chapters.values()]

    data = [list(map(itemgetter(*skey), chapter)) for skey, chapter 
                in zip(sub_chaper_keys, logbook.chapters.values())]
    
    tmp = []
    for a in zip(*data):
        flat = []
        for e in a:
            flat+=[*e]
        tmp.append(flat)
    data = np.array(tmp)

    columns = reduce(add, [["_".join([x, y]) for y in s] 
                        for x, s in zip(chapter_keys, sub_chaper_keys)])
    df = pd.DataFrame(data, columns=columns)

    keys = logbook[0].keys()
    data = [[d[k] for d in logbook] for k in keys]
    for d, k in zip(data, keys):
        df[k] = d
    return df

def basic_budget_scheduler(gen_threshold):# list of tuple (generation, simulation_budget)
    def scheduler(gen, population, simulation_budget, parallel_update):
        for g, v in reversed(gen_threshold):
            if gen>g:
                return v, parallel_update
        return simulation_budget, parallel_update
    return scheduler

def save_each_generation(path, modulo=10):
    def save(i, pop, hof, logbook):
        data = {"pop":pop, "hof":hof}  
        if i%modulo == 0:
            with open(os.path.join(path,"data-"+str(i)+".pkl"), "wb") as input_file:
                pickle.dump(data, input_file, pickle.HIGHEST_PROTOCOL)
    return save
