# Copyright (c) Mathurin Videau. All Rights Reserved.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import numpy as np
import random
from deap import gp
from operator import attrgetter

operator_complexity = {
    'add':1 , 'substract':1, 'const':1, 'multiply':1, 'divide':2, 'abs':2, 'or_':4, 'and_':4, 'gt':4, 'if_then_else':5, 'tanh':4, 'tan':4,
    'cos':4, 'sin':4, 'power':3, 'cube':3, 'square':3, 'sqrt':3, 'inv':2, 'log':3, 'exp':3
}

def complexity(individual):
    return sum(map(lambda x: operator_complexity.get(x.name, 1), individual))

def div(x1, x2):
    if isinstance(x1, float) or x1.ndim == 0:
        out = np.full_like(x2, 10_000)
    else:
        out = np.full_like(x1, 10_000)
    return np.divide(x1, x2, where=np.abs(x2) > 0.0001, out=out)

def if_then_else(cond, true, false):
    return np.where(cond, true, false)

def classification(*args):
    return np.argmax(args)

def intervales(x):
    if x>0.33:
        return 1
    elif x<-0.33:
        return -1
    return 0

def exp(x):
    return np.exp(x, where=np.abs(x)<32, out=np.full_like(x, 100_000))

def log(x):
    return np.log(np.abs(x), where=np.abs(x) > 0.00001,  out=np.full_like(x, 10_000))

def power(x, n):
    with np.errstate(over='ignore', divide='ignore',  invalid='ignore'):
        if n < 0.0:
            return np.where(np.logical_and(np.abs(x) < 1e6, np.abs(x)>0.001), np.sign(x) * (np.abs(x)) ** (n), 0.0)
        return np.where(np.abs(x) < 1e6, np.sign(x) * (np.abs(x)) ** (n), 0.0)

def ephemeral_mut(individual, mode, mu=0, std=1):#mutation of constant
    ephemerals_idx = [index for index, node in enumerate(individual) if isinstance(node, gp.Ephemeral)]
    if len(ephemerals_idx) > 0:
        if mode == "one":
            ephemerals_idx = (random.choice(ephemerals_idx),)
        for i in ephemerals_idx:
            new = type(individual[i])()
            new.value = individual[i].value + random.gauss(mu, 0.1*abs(individual[i].value))
            individual[i] = new
    return individual,

def mutate(individual, expr=None, pset=None, mode="one", mu=0, std=1):#mutate that include constant and operation modifications
    #mut = gp.mutEphemeral(mut, mode)
    if random.random() < 0.3:
        return ephemeral_mut(individual, mode=mode, mu=mu, std=std)
    else:
        return gp.mutUniform(individual, expr=expr, pset=pset)

def selQDRandom(grid, k, cellSelection=random.choice):
    idxs = [key for key, v in grid._solutions.items() if v]
    return [cellSelection(grid._solutions[random.choice(idxs)]) for _ in range(k)]

def selFitProp(individuals, fit_attr="fitness"):#cell fitness proportionnal selection
    min_fit = getattr(min(individuals, key=attrgetter(fit_attr)), fit_attr).values[0]
    sum_fits = sum((getattr(ind, fit_attr).values[0] - min_fit) for ind in individuals)
    u = random.random() * sum_fits
    sum_ = 0
    for ind in individuals:
        sum_ += getattr(ind, fit_attr).values[0] - min_fit
        if sum_ > u:
            return ind
    return ind
