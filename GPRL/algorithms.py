# Copyright (c) Mathurin Videau. All Rights Reserved.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

from collections import deque
from deap import tools, algorithms
import heapq
from timeit import default_timer as timer

from .UCB import UCBFitness, HeapWithKey

#/!\ stochastique objective must be placed first !
def eaMuPlusLambdaUCB(population, toolbox, simulation_budget, parallel_update, mu, lambda_, cxpb, mutpb, ngen,
                   select=False, stats=None, halloffame=None, verbose=__debug__, budget_scheduler=None, iteration_callback=None):
    assert all([isinstance(ind.fitness, UCBFitness) for ind in population])

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.add_reward(fit[0])
        ind.fitness.values = ind.fitness.calc_fitness(simulation_budget), *fit[1:]

    if select:
        get_individual = lambda inds: toolbox.select(inds, parallel_update)
    else:
        popoff = HeapWithKey(population, lambda x: -x.fitness.values[0])
        get_individual = lambda _: [popoff.pop() for _ in range(parallel_update)]
    tmp = 0
    while(tmp < simulation_budget):
        inds = get_individual(population)
        fitnesses = toolbox.map(toolbox.evaluate, inds)
        for ind, fit in zip(inds, fitnesses):
            ind.fitness.add_reward(fit[0])
            ind.fitness.values = ind.fitness.calc_fitness(simulation_budget), *fit[1:]  
            if not select:
                popoff.push(ind)
        tmp+=1

    population = toolbox.select(population, mu)

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats is not None else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)
    if iteration_callback is not None:
        iteration_callback(0, population, halloffame, logbook)

    # Begin the generational process
    for gen in range(1, ngen + 1):
        if budget_scheduler is not None:
            simulation_budget, parallel_update = budget_scheduler(gen, population, simulation_budget, parallel_update)
        # Vary the population
        offspring = algorithms.varOr(population, toolbox, lambda_, cxpb, mutpb)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.reset()
            ind.fitness.add_reward(fit[0])
            ind.fitness.values = ind.fitness.calc_fitness(simulation_budget), *fit[1:]

        for ind in population:
            ind.fitness.update_offset()
        
        popoff = population+offspring
        if not select:
            popoff = HeapWithKey(popoff, lambda x: -x.fitness.values[0])
            get_individual = lambda _: [popoff.pop() for _ in range(parallel_update)]
        
        tmp = 0
        while(tmp < simulation_budget):
            inds = get_individual(popoff)
            fitnesses = toolbox.map(toolbox.evaluate, inds)
            for ind, fit in zip(inds, fitnesses):
                ind.fitness.add_reward(fit[0])
                ind.fitness.values = ind.fitness.calc_fitness(simulation_budget), *fit[1:]
                if not select:
                    popoff.push(ind)
            tmp+=1

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Select the next generation population
        population[:] = toolbox.select(population + offspring, mu)

        # Update the statistics with the new population
        record = stats.compile(population) if stats is not None else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)
        if iteration_callback is not None:
            iteration_callback(gen, population, halloffame, logbook)

    return population, logbook

# "Deapified" version of :
# Regularized Evolution for Image Classifier Architecture Search ; https://ojs.aaai.org/index.php/AAAI/article/view/4405
# based code from : https://github.com/google-research/google-research/tree/master/evolution/regularized_evolution_algorithm
def regularized_evolution(population, toolbox,  mu, lambda_, cxpb, mutpb, cycles, stats=None, halloffame=None, verbose=__debug__):
    """Algorithm for regularized evolution (i.e. aging evolution).

    Follows "Algorithm 1" in Real et al. "Regularized Evolution for Image
    Classifier Architecture Search".

    Args:
    cycles: the number of cycles the algorithm should run for.

    Returns:
    history: a list of `Model` instances, representing all the models computed
        during the evolution experiment.
    """
    assert mu <= len(population)
    if isinstance(deque, population):
        remove = lambda pop: pop.popleft()
    else:
        remove = lambda pop: pop.pop(0)
    
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit
    
    record = stats.compile(population) if stats is not None else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    history = []  # Not used by the algorithm, only used to report results.

    # Carry out evolution in cycles.
    while len(history) < cycles:
        parents = toolbox.select(population, k=mu)

        # Create offspring and store it.
        offspring = algorithms.varOr(parents, lambda_, cxpb, mutpb)

        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
        
        population.extend(offspring)
        history.extend(offspring)

        # Withdrawal of oldest individuals
        for _ in range(len(invalid_ind)):
            remove(population)
        
        record = stats.compile(population) if stats is not None else {}
        logbook.record(gen=len(history), nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)
        
    return history, logbook


#objectif is at fitness.fit[0], fitness.fit[1:] is for features.
def qdLambda(init_batch, toolbox, container, batch_size, ngen, lambda_, cxpb = 0.0, mutpb = 1.0, stats = None, halloffame = None, verbose = False, show_warnings = False, start_time = None, iteration_callback = None):
    """The simplest QD algorithm using DEAP.
    :param init_batch: Sequence of individuals used as initial batch.
    :param toolbox: A :class:`~deap.base.Toolbox` that contains the evolution operators.
    :param batch_size: The number of individuals in a batch.
    :param niter: The number of iterations.
    :param stats: A :class:`~deap.tools.Statistics` object that is updated inplace, optional.
    :param halloffame: A :class:`~deap.tools.HallOfFame` object that will
                       contain the best individuals, optional.
    :param verbose: Whether or not to log the statistics.
    :param show_warnings: Whether or not to show warnings and errors. Useful to check if some individuals were out-of-bounds.
    :param start_time: Starting time of the illumination process, or None to take the current time.
    :param iteration_callback: Optional callback funtion called when a new batch is generated. The callback function parameters are (iteration, batch, container, logbook).
    :returns: The final batch
    :returns: A class:`~deap.tools.Logbook` with the statistics of the
              evolution
    """
    if start_time is None:
        start_time = timer()
    logbook = tools.Logbook()
    logbook.header = ["iteration", "containerSize", "evals", "nbUpdated"] + (stats.fields if stats else []) + ["elapsed"]

    if len(init_batch) == 0:
        raise ValueError("``init_batch`` must not be empty.")

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in init_batch if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit[0],
        ind.features = fit[1:]

    if len(invalid_ind) == 0:
        raise ValueError("No valid individual found !")

    # Update halloffame
    if halloffame is not None:
        halloffame.update(init_batch)

    # Store batch in container
    nb_updated = container.update(init_batch, issue_warning=show_warnings)
    if nb_updated == 0:
        raise ValueError("No individual could be added to the container !")

    # Compile stats and update logs
    record = stats.compile(container) if stats else {}
    logbook.record(iteration=0, containerSize=container.size_str(), evals=len(invalid_ind), nbUpdated=nb_updated, elapsed=timer()-start_time, **record)
    if verbose:
        print(logbook.stream)
    # Call callback function
    if iteration_callback is not None:
        iteration_callback(0, init_batch, container, logbook)

    # Begin the generational process
    for i in range(1, ngen + 1):
        start_time = timer()
        # Select the next batch individuals
        batch = toolbox.select(container, batch_size)

        ## Vary the pool of individuals
        offspring = algorithms.varOr(batch, toolbox, lambda_, cxpb=cxpb, mutpb=mutpb)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit[0],
            ind.features = fit[1:]

        # Replace the current population by the offspring
        nb_updated = container.update(offspring, issue_warning=show_warnings)

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(container)

        # Append the current generation statistics to the logbook
        record = stats.compile(container) if stats else {}
        logbook.record(iteration=i, containerSize=container.size_str(), evals=len(invalid_ind), nbUpdated=nb_updated, elapsed=timer()-start_time, **record)
        if verbose:
            print(logbook.stream)
        # Call callback function
        if iteration_callback is not None:
            iteration_callback(i, batch, container, logbook)

    return container, logbook
