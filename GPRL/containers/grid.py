# Copyright (c) Mathurin Videau. All Rights Reserved.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

from functools import reduce
import operator
import numpy as np
from qdpy.containers import Grid

class FixGrid(Grid):# fix a bug in qdpy 0.1.2.1  with Deep grid (nb_items_per_bin)
    def _init_grid(self) -> None:
        """Initialise the grid to correspond to the shape `self.shape`."""
        self._solutions = {x: [] for x in self._index_grid_iterator()}
        self._nb_items_per_bin = np.zeros(self._shape, dtype=int) #{x: 0 for x in self._index_grid_iterator()}
        self._fitness = {x: [] for x in self._index_grid_iterator()}
        self._features = {x: [] for x in self._index_grid_iterator()}
        self._quality = {x: None for x in self._index_grid_iterator()}
        self._quality_array = np.full(self._shape + (len(self.fitness_domain),), np.nan)
        self._bins_size = [(self.features_domain[i][1] - self.features_domain[i][0]) / float(self.shape[i]) for i in range(len(self.shape))]
        self._filled_bins = 0
        self._nb_bins = reduce(operator.mul, self._shape)
        self.recentness_per_bin = {x: [] for x in self._index_grid_iterator()}
        self.history_recentness_per_bin = {x: [] for x in self._index_grid_iterator()}
        self.activity_per_bin = np.zeros(self._shape, dtype=float)
